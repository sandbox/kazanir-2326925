<?php

/**
 * @file
 * Abstract and interface plugin implementation.
 */

interface DndLevelInterface extends EntityBundlePluginProvideFieldsInterface {

  /**
   * Provides the level form.
   *
   * @param $form
   *   The level form. Might be embedded in another form through
   *   Inline Entity Form.
   * @param $form_state
   *   The form state of the complete form.
   *   Always use drupal_array_get_nested_value() instead of accessing
   *   $form_state['values'] directly.
   */
  public function form(&$form, &$form_state);

  /**
   * Validates the level form.
   *
   * @param $form
   *   The level form. Might be embedded in another form through
   *   Inline Entity Form, so form['#parents'] needs to be taken into account
   *   when fetching values and setting errors.
   * @param $form_state
   *   The form state of the complete form.
   *   Always use drupal_array_get_nested_value() instead of accessing
   *   $form_state['values'] directly.
   */
  public function formValidate($form, &$form_state);

  /**
   * Submits the level form.
   *
   * @param $form
   *   The level form. Might be embedded in another form through
   *   Inline Entity Form, so form['#parents'] needs to be taken into account
   *   when fetching values and setting errors.
   * @param $form_state
   *   The form state of the complete form.
   *   Always use drupal_array_get_nested_value() instead of accessing
   *   $form_state['values'] directly.
   */
  public function formSubmit(&$form, &$form_state);

  /**
   * Determine whether this entity should grant access to a given field form widget.
   *
   * Access to various fields typically depends on $level as well as the $class plugin.
   *
   * @param $fieldname
   *    The machine name of the field from the Field API.
   *
   * @return bool $access
   */
  public function fieldAccess($fieldname);

}

abstract class DndLevel extends Entity implements DndLevelInterface, EntityBundlePluginValidableInterface {

  public $id;
  public $vid;
  public $class;        // Machine name of the class. Also serves as the bundle key for the entity.
  public $label;        // Normally combined out of the $class and $class_level to be e.g. "Wizard 6"
  public $created;
  public $changed;
  public $cid;          // Reverse reference to the character this level is attached to.
  public $xp_level;     // The XP level this Level entity represents in the character's overall total.
  public $class_level;  // The class level of this Level entity i.e. the "4" in "Fighter 4".

  public function form(&$form, &$form_state) {

    $existing_levels = array_map(function ($entity_array) { return $entity_array['entity']; }, $form_state['inline_entity_form'][$form['#ief_id']]['entities']);

    if ($form_state['#creation_mode'] == 'dm') {
      $class_levels = range(0,20);
      unset($class_levels[0]);

      $form['class_level'] = array(
        '#type' => 'select',
        '#options' => $class_levels,
        '#default_value' => ($this->class_level ?: $this->defaultClassLevel($existing_levels)),
        '#title' => t('Class Level'),
        '#description' => t('Select the level of the character in this class.'),
      );
      $form['xp_level'] = array(
        '#type' => 'select',
        '#options' => $class_levels,
        '#default_value' => ($this->xp_level ?: $this->defaultXpLevel($existing_levels)),
        '#title' => t('Experience Level'),
        '#description' => t('Select the experience level of the character.'),
      );
    }
    else {
      $form['class_level'] = array(
        '#type' => 'hidden',
        '#value' => ($this->class_level ?: $this->defaultClassLevel($existing_levels)),
        '#title' => t('Class Level'),
      );
      $form['xp_level'] = array(
        '#type' => 'hidden',
        '#value' => ($this->xp_level ?: $this->defaultXpLevel($existing_levels)),
        '#title' => t('Experience Level'),
      );
      $class_label = dnd_class_load($this->class)->label;
      $form['levels'] = array(
        '#type' => 'markup',
        '#markup' => t('You are adding @class level @cl at character level @xpl', array('@class' => $class_label, '@cl' => $form['class_level']['#value'], '@xpl' => $form['xp_level']['#value'])),
      );
    }

    field_attach_form('dnd_level', $this, $form, $form_state);

    return $form;
  }

  public function formValidate($form, &$form_state) {
    field_attach_form_validate('dnd_level', $this, $form, $form_state);
  }

  public function formSubmit(&$form, &$form_state) {
    $level_values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $this->class_level = $level_values['class_level'];
    $this->xp_level = $level_values['xp_level'];

    $this->cid = $form_state['dnd_character']->cid;
    $this->label = dnd_class_load($this->class)->label . ' ' . $this->class_level;

    field_attach_submit('dnd_level', $this, $form, $form_state);
  }

  public function defaultClassLevel(array $existing_levels) {
    $current_class_level = max(array_map(function ($level) { return ($level->class == $this->class ? $level->class_level : 0); }, $existing_levels));
    return $current_class_level + 1;
  }

  public function defaultXpLevel(array $existing_levels) {
    $current_xp_level = max(array_map(function ($level) { return $level->xp_level; }, $existing_levels));
    return $current_xp_level + 1;
  }

  public function fieldAccess($fieldname) {
    return TRUE;
  }

  static public function fields() {
    return array();
  }

  static public function isValid() {
    return TRUE;
  }

}

class DndLevelBroken extends DndLevel {

}

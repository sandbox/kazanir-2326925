<?php

/**
 * @file
 * Abstract and interface plugin implementation.
 */
 
interface DndPowerInterface extends EntityBundlePluginProvideFieldsInterface {

}

abstract class DndPower extends Entity implements DndLevelInterface, EntityBundlePluginValidableInterface {

}

class DndPowerBroken extends DndLevel {

}

<?php

//          Level: Each class plugin
//          Level: Fields methods for each class plugin
//          Level: Field access for each class plugin
//          Level: Metadata controller

//          Class: Add fields (description, hit dice, ???)
//          Class: Metadata controller
//          Class: Evaluate UI controller
//          Class: UI controller -- how to handle non-PC bundles?

//          Character: expanded entity class methods / calculation
//          Character: Features + Race field setup???
//          Character: Form field validator for missing/misplaced levels in DM-mode IEF
//          Character: UI controller / views / etc
//          Character: Real views + access control hooks / permissions

//          Feature: UI controller + list view + ??? exportable + bundles is a pain
//          Feature: Patched entityreference for tokenized views calls
//          Feature: Tokenized arg view for type + level
//          Feature: Metadata controller

//          Power: Set up EBP entity info hook
//          Power: Initial plugins for each type
//          Power: Power types callback
//          Power: IEF controller
//          Power: Schema hook
//          Power: Metadata controller

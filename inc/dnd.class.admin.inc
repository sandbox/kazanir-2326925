<?php

/**
 * @file
 * D&D class editing UI.
 */

/**
 * UI controller.
 */
class DndClassUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = t('Manage D&D classes, including the fields attached to a class level entity.');
    return $items;
  }
  
}

/**
 * Generates the class editing form.
 */
function dnd_class_form($form, &$form_state, DndClass $class, $op = 'edit') {

  if ($op == 'clone') {
    $class->label .= ' (cloned)';
    $class->type = '';
  }

  $form['label'] = array(
    '#title' => t('Class Name'),
    '#type' => 'textfield',
    '#default_value' => $class->label,
    '#description' => t('The human-readable name of this class.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable class name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $class->name,
    '#maxlength' => 32,
    '#disabled' => $class->hasStatus(ENTITY_IN_CODE) && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'dnd_class_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this class. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Class Type'),
    '#default_value' => !empty($class->type) ? $class->type : 'pc',
    '#required' => TRUE,
    '#options' => array_combine(array_keys(dnd_class_types()), array_map(function($type) { return $type['label']; }, dnd_class_types())),
    '#weight' => -20,
    '#description' => t('Select the type of character class. The default and only initial option are player character classes.'),
  );

  field_attach_form('dnd_class', $class, $form, $form_state);
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Class'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Class'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('dnd_class_form_submit_delete'),
    '#access' => !$class->hasStatus(ENTITY_IN_CODE) && $op != 'add' && $op != 'clone'
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function dnd_class_form_submit(&$form, &$form_state) {
  $class = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $class->save();
  $form_state['redirect'] = 'admin/dnd/classes';
}

/**
 * Form API submit callback for the delete button.
 */
function dnd_class_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/dnd/classes/' . $form_state['dnd_class']->name . '/delete';
}

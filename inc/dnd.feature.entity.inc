<?php

/**
 * @file Provides entity class for class features.
 */

class DndFeature extends Entity {

  public $id;
  public $name = '';
  public $label = '';
  public $type = '';
  public $data;
  public $description;

}
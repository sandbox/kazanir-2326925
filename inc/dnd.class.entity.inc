<?php

/**
 * @file Provides entity class for character classes.
 */

class DndClass extends Entity {

  public $id;
  public $name = '';
  public $label = '';
  public $type = 'pc';
  public $data;
  
  public function getLevelPlugin() {
    $plugins = dnd_get_level_plugins();
    if (isset($plugins[$this->name])) {
      return $plugins[$this->name];
    }
    else {
      return FALSE;
    }
  }

}
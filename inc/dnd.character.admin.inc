<?php

/**
 * @file
 * D&D character editing UI.
 */

/**
 * UI controller.
 */
class DndCharacterUIController extends EntityBundleableUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = t('Manage D&D characters.');
    return $items;
  }
  
  
}

function dnd_character_form($form, &$form_state, DndCharacter $char, $op = 'edit') {
  if (!isset($form_state['#creation_mode'])) {
    $form_state['#creation_mode'] = 'player';
  }
  $form['type'] = array('#type' => 'value', '#value' => $char->type);
  
  $form['creation_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Creation mode'),
    '#description' => t('Dungeon Master mode will allow you to add levels out of sequence and otherwise create not-table-legal characters.'),
    '#options' => array(
      'player' => t('Player'),
      'dm' => t('Dungeon Master'),
    ),
    '#default_value' => $form_state['#creation_mode'],
    '#weight' => -50,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Character Name'),
    '#default_value' => isset($char->name) ? $char->name : '',
    '#required' => TRUE,
    '#weight' => -15,
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  field_attach_form('dnd_character', $char, $form, $form_state);
  
  return $form;
}

function dnd_character_form_validate(&$form, &$form_state) {
  $form_state['#creation_mode'] = (isset($form_state['values']['creation_mode']) ? $form_state['values']['creation_mode'] : 'player');

  entity_form_field_validate('dnd_character', $form, $form_state);
}

function dnd_character_form_submit(&$form, &$form_state) {
  $char = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $char->save();
  $info = $char->entityInfo();
  $form_state['redirect'] = 'admin/dnd/characters';
}
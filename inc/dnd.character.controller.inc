<?php

class DndCharacterController extends EntityAPIController {

  /**
   * Overridden to add in more default values.
   */
  public function create(array $values = array()) {
    $values += array(
      'created' => REQUEST_TIME,
      'uid' => $GLOBALS['user']->uid,
      'type' => 'pc'
    );
    return parent::create($values);
  }



}


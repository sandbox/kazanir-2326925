<?php

/**
 * @file
 * D&D feature editing UI.
 */

/**
 * UI controller.
 */
class DndFeatureUIController extends EntityBundleableUIController {

  /**
   * Overrides hook_menu() defaults.
   *
   * @todo: Fix this. The content UI controller sucks a fat one.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    return $items;
  }

}

/**
 * Generates the feature editing form.
 */
function dnd_feature_form($form, &$form_state, DndFeature $feature, $op = 'edit') {

  if ($op == 'clone') {
    $feature->label .= ' (cloned)';
    $feature->type = '';
  }

  $form['label'] = array(
    '#title' => t('Feature Name'),
    '#type' => 'textfield',
    '#default_value' => $feature->label,
    '#description' => t('The human-readable name of this feature.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable feature name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $feature->name,
    '#maxlength' => 32,
    '#disabled' => $feature->hasStatus(ENTITY_IN_CODE) && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'dnd_feature_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this feature. It must only contain lowercase letters, numbers, and underscores.'),
  );

  field_attach_form('dnd_feature', $feature, $form, $form_state);
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Feature'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Feature'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('dnd_feature_form_submit_delete'),
    '#access' => !$feature->hasStatus(ENTITY_IN_CODE) && $op != 'add' && $op != 'clone'
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function dnd_feature_form_submit(&$form, &$form_state) {
  $feature = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $feature->save();
  $form_state['redirect'] = 'admin/dnd/features';
}

/**
 * Form API submit callback for the delete button.
 */
function dnd_feature_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/dnd/features/' . $form_state['dnd_feature']->name . '/delete';
}

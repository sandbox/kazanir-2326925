<?php

/**
 * @file Inline entity form controller for DndLevel entities.
 */

class DndLevelIEFController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = array();
    $fields['xp_level'] = array(
      'type' => 'property',
      'label' => t('Character Level'),
      'weight' => 1,
    );
    $fields['label'] = array(
      'type' => 'property',
      'label' => t('Class Level'),
      'weight' => 2,
    );

    return $fields;
  }

  /**
   * Overrides EntityInlineEntityFormController::defaultSettings().
   */
  public function defaultSettings() {
    $defaults = parent::defaultSettings();

    // Level entities are always deleted if their character is removed, they
    // are never managed alone.
    $defaults['delete_references'] = TRUE;

    return $defaults;
  }

  /**
   * Overrides EntityInlineEntityFormController::settingsForm().
   */
  public function settingsForm($field, $instance) {
    $form = parent::settingsForm($field, $instance);

    // Adding existing entities is not supported for levels.
    $form['allow_existing']['#access'] = FALSE;
    $form['match_operator']['#access'] = FALSE;

    return $form;
  }

  public function entityForm($entity_form, &$form_state) {
    $info = entity_get_info('dnd_level');
    $level = $entity_form['#entity'];

    $entity_form = $level->form($entity_form, $form_state);

    return $entity_form;
  }
  
  public function entityFormValidate($entity_form, &$form_state) {
    $level = $entity_form['#entity'];
    $level->formValidate($entity_form, $form_state);  
  }
  
  public function entityFormSubmit(&$entity_form, &$form_state) {
    $level = $entity_form['#entity'];
    $level->formSubmit($entity_form, $form_state);    
  }
  
  

}
